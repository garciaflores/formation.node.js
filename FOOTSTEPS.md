# Diverse links and notes

[MVC with Express, good structure](https://itnext.io/a-new-and-better-mvc-pattern-for-node-express-478a95b09155)

[DisplayHelper with CSS and Vuetify](https://vuetifyjs.com/en/styles/display/)
For exemple `class='hidden-sm-and-down'` will only be rendered for screen classified at least as md (>960px)

[MozillaTutorial - Routes and controller](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes)
Present a different structure having a more precise distinction between the routes and the API, and the logic that will be used. 
It is quite elegant.
I think it is better if you have a very large API.

#### Vue shorthands:

>v-slot:header -> #header

>v-bind:href="url" -> :href:"url"

>v-on:click="doSomething" -> @click="doSomething"


#### Basic Structure

```
/
|- models/ -> models for mongoose, PascalCase.js
|- controllers/ -> controllers for REST API, camelCase.js
|- src/ -> Vue related files
    |- components/ -> single-page Vue components, PascalCase.js
    |- router/ -> VueRouter files (index.js in our example)
    |- App.vue -> main Vue file
    |- main.js -> root Vue instance
|- server.js -> express backend instance, and connection to our DB
|- config/ -> configuration files, such as DB location
```


# Installation

## Basic Vue

### Install @vue/cli and vuetify (stylish UI)

```bash
$ vue create myapp
$ cd myapp
$ vue add vuetify
```
Copy a template into App.vue, make small adjustments (for example add drawer to data)

***

## Express


Install express in the working directory and add it to the dependencies list:
```bash
$ npm install express --save 
```
Create a file in the __root directory__ that will be the home of our express instance.
We'll call that file `server.js` (called `app.js` in express doc).

``` javascript
const express = require('express');

// Create an instance of express
const app = express();
// In the book : const port = process.env.API_PORT || 8081;
const port = 3000;

// When there is a GET request in the root URL, send Hello World
app.get('/', (req, res) => res.send('Hello World'));
// Listen for connection on the specified port
app.listen(port, () => console.log(`App listening at http://localhost:${port}`));
```

***

## MongoDB

MongoDB is an open source NoSQL database that uses document-oriented data model.

If you are used to SQL, this is the terminology of MongoDB.
>Tables -> Collections

>Rows -> Documents

### Install MongoDB

MongoDB runs using mongod user account and uses following default directories

`/var/lib/mongo`

`/var/log/mongodb`

It will be necessary to start mongo using _sudo_ or login as _root_.
```bash
$ sudo mongod #(start a server on default port 27017)
$ mongo #(CLI to see and operate on DBS, >help for instructions on how to use it)
```
***

## Mongoose 

Mongoose is a front-end to MongoDB.

```bash
$ npm install mongoose --save
```

You can create a file in `/config/` to export the name of the DB

``` javascript
// /config/Config.js
module.exports = {
  DB: 'mondodb://localhost/my_db_name',
}
```

Then in `server.js`,   add these lines:

``` javascript
const mongoose = require('mongoose');
const config = require('./config/Config);

// Connect to the mongoDB
mongoose.connect(config.DB, () => {
    console.log('Connection has been made');
})
.catch(err => {
    console.error('App starting error:', err.stack);
    process.exit(1);
});
```

To start our application, we will need to __have the mongo server active__:
```bash
$ sudo mongod
```


# Next

## MongoDB Models

A database can be composed of several collections (or table).
For each collection, you will specify what type of data you want to add as a document (row).
This is called a Model and looks like a struct.

With Mongoose, we will create Schemas that will be compiled as Mongo Models:

```javascript
// at /models/Movies.js
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
// A movie is composed of four attributes which are:
const MovieSchema = new Schema({
    name: String,
    description: String,
    release_year: Number,
    genre: String,
});
// Now compile it as a model
const Movie = mongoose.model('Movie', MovieSchema);

// Export it so it can be retrieved thereafter to create a new document
// and add it to the collection.
module.exports = Movies;
```

You can have more complex specifications for the Schema, such as default values, complex structures, min/max, required....
It is also possible to define methods, which are function that can be called. For exemple we could create a method print to return a string containing the movie information.
Check [Mongoose Documntation](https://mongoosejs.com/docs/index.html) for more information!

****

## Component

A vue component is a reusable vue instance that can be rendered.
These will be the building blocks of your application.

`App.vue` is what tells how the overlay of your main page will be rendered.
You will put components depending on your routes (localhost/movies for exemple).

It is possible to have nested components. For exemple, you can have the component __Movie__ that will be a card displaying a movie and another component __ShowMovies__ that will render a list of all the movies in your db, using thus the component __Movie__.

Components are placed in `/src/components`. They are split in two parts:

* The _template_: how it will render
* The _script_: what operation it might run and what variables it might contains

***

## VueRouter

VueRouter lets you map a component to the routes and let Vue know where to render them.

Install it:
```bash
$ npm install vue-router --save
```

You can use it in your applications to have links to another part of your websites, but also to render the corresponding __Components__ at the specified location.

```HTML
<-- In App.vue -->

<-- router-link for navigation, rendered as an <a> -->
<router-link to="/foo">Go to foo</router-link>

<-- component matched by the route will be rendered here
Usually put inside the <div id="app"> -->
<router-view></router-view>
```

For that to work, we need to make it explicit which __route__ maps to which __element__:


```javascript
// In /src/router/index.js

import Vue from 'vue';
import VueRouter from 'vue-router';

// These are components, thus contain Vue Template to be rendered
// '@' means /src/ (resolved in build/webpack.base.conf.js)
import ShowMovies from '@/components/Movie';
import Home from '@/components/Home';

Vue.use(VueRouter);

const router = new VueRouter({
   // mode: 'history', if you don't want # in your URL, see documentation.
   routes: [
     {
       path: '/',
       name: 'Home',
       component: Home,
     },
     {
       path: '/movies/', // NEVER FORGET THE FIRST / !
       name: 'ShowMovies',
       component: ShowMovies,
     }]
});
```

Then we can export router, and mount it into our root Vue instance.

```javascript
// In src/main.js
const router = require('./router');

new Vue({
   vuetify,
   router,
   render: h => h(App),
}).$mount('#app")
```

We can now access to the router inside any component with `this.$router` and `this.$route`.

In a component script for exemple you can redirect with
`this.$router.push({name: 'NewRoute'})`.

***

[VueRouter documentation](https://router.vuejs.org/)

[VueRoute history mode](https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations): Removes the # in the URL

# Model-View-Controller

The _Model-View-Controller_, or _MVC_, is an architectural pattern that might helps you organize your project. There is three main components linked together that each have a specific purpose.

- The Model (here MongoDB) updates the View based on its content.
- The View (template engine pug) renders for the user to see.
- The User uses the API when browsing the application.
- The Controller handles the API and manipulates the Model accordingly.
- The Model then updates the View, and the loop goes on and on.


(Copied)
Model: Part of app that will deal with DB or any data-related functionality
View: Everything the user sees, pages that we're sending to the client
Controller: The logic of the app. Calls models to get data then put the data to the views.


## Forms with v-form

[v-form documentation](https://vuetifyjs.com/en/components/forms/)

Let's create a form.
Vuetify's v-form allows us to create forms and specify rules for the input fields.


```javascript
<template>
  <v-content>
    <v-form
      ref="form"
      v-model="valid"
      lazy-validation
    >
      <v-text-field
        label="text_field_name"
        v-model="text_field_variable"
        v-bind:rules="text_field_rules"
        required
      ></v-text-field>
    </v-form>
  </v-content>
</template>
```

### \<v-form> options

- _lazy-validation_:
If enabled value (boolean representing the validity of the form) is true unless there is visible validations errors

- _v-model="valid"_: connects the value of that component to the value "valid" in the script part of the component.

- _ref="forms"_: form has 3 functions that can be accessed setting a ref on the component. The ref lets us access these internal functions: 
  -  `this.$refs.form.validate()`
  - >Will validate all inputs and return if they're all valid or not
  - `this.$refs.form.reset()`
  - > Will clear all inputs and their validation errors
  - `this.$refs.form.resetValidation()`
  - > Will only reset input validation (but not alter their state)


### \<v-text-field> options

- _v-model_: connects again the value of that text field, ie what the user will type to the data in the script of the component.
- _v-bind:rules_: get the rules defined in data of the script part to define what is considered valid for this textfield.
- _label_: the value here will be written in the field. Can be used to indicate valid input.

v-model and v-bind have a similar usage. It connects a value in a tag to a value in the data. v-bind allows only one-way binding, from the data to the tag value. With v-model, a change in the tag value will also change the data.

### Now the script part

```javascript
<script>
export default {
  data: () => ({
    valid: true,
    name: '',
    nameRules: [ 
      v => !!v || 'Movie Name is required',
      v => (v && v.length <= 100) || 'Names must be less than 100 characters'
    ],
  }),
  methods: {
    validate() {
      this.$refs.form.validate()
    },
  },
};
</script>

// And adding just after the </v-form> a button:

<v-btn :disabled="!valid" color="success" @click="validate">Validate</v-btn>

```

In __data__, we chose the initialized values of the variable we will need for the logic of our Component.
__Don't forget that data is a function that returns an object !__
Notice nameRules which is an array of functions that will validate a text field.

Then we have __methods__ which is an object containing all the functions we might need for the logic.

In the button we created, we have `:disabled="!valid"` ( " : " is a shorthand for "v-bind:", cf shorthands). Thus we bound the value `!valid` to the attribute `disabled`.
That means that the button will not be activable if the value `valid` (which is bound to the v-form thanks to v-model) is _false_.

`@click="validate"` means that when we click on the button, it will call the method `validate()` which then verify all the fields, `@click` is a shorthand for `v-on:click`.

For now, that button does not do anything except verifying the user inputs.

## Posting that form

Once the form is validated, we will submit it so a movie can be created and then added to our mongoDB collection.

For that, we will need to follow these steps: 

1. Make a POST Request with __axios__ containing the parameters of our movie
2. Receive it with our back-end API
3. Create a movie schema with the parameters
4. Add it as a document in the collection

### 1 - Make a POST Request with axios

Notes:
For submit function, I don't know why it is async in the book. And I don't know why it have to return true if the form does not validate.

First, we install axios, then import it in the first line of our script:
```bash
$ npm install --save axios
```
```javascript
import axios from 'axios';
```

Then we add a new method submit and a button to use that method.

``` html
// In template after the form
// Instead of validate button
<v-btn :disabled="!valid" @click="submit">Submit</v-btn>
```
```javascript
// In method in the script
async submit() {
  if (this.$refs.form.validate()) {
    // Axios will create the HTTP Request that will be received by our API
    return axios({
      // What type of REST method we want to send. Check REST APIs for more info
      method: 'post',
      // What is the data we want to send in the request.
      // Here we want to send the inputs of the form.
      data: {
        name: this.name,
      },
      // Where we send our request. We will need to specify in our API what
      // does a 'post' in /movies does.
      url: '/movies',
      //
      header: {
        'Content-Type': 'application/json'
      },
    })
    // If success, then...
    .then(() => {
      console.log('POST Request to add a new movie successful');
      this.$refs.form.reset();
      this.$router.push({name: 'Home'}); // Going home since we succeeded!
    })
    // If failure, catch...
    .catch((error) => {
      message = error.response.data.message;
      console.error(message);
    });
  }
  return true;
}

```

### 2 - Receiving the POST Request with our back-end

We will now create a folder called `/controllers`.
Since we might need several REST commands for different parts of our website, for exemple:
Movies:
Adding movies
Getting a movie
Deleting a movie
Updating a movie
User:
Creating a new user
Deleting a user
Getting a user

So the folder `/controllers` will create separate files so it will be easier and clearer to maintain.

Thus we will create in that folder `movie.js`.

```javascript
const Movie = require('../models/Movie.js'); // Mongoose models

module.exports.controller = (app) => {
  // Add a new movie
  app.post('/movies', (req, res) => {
    // Create the model for the movie to add
    const newMovie = Movie({
      name: req.body.name,
      description: req.body.description,
      release_year: req.body.release_year,
      genre: req.body.genre,
    });
    // Save that movie to our collection
    newMovie.save((error, movie) => {
      if (error) { console.error(error); }
      res.send(movie); // We send the movie as the HTTP Response
    });
  });
};
```

That file will specify all the possible REST commands we want to implement for that model as well as the logic that will make it work.
>You can check the mozilla tutorial on top of the file for a different way of building controllers by separating the requests and the logic

This file alone does not connect to the router we wrote in `server.js`. Thus we need a way to connect it.

To do that, we will reach each files in `/controllers` and add it to the router in `server.js`:

```javascript
// In server.js, add these lines (be careful where you put it
// File system reader, you'll have to install it, you know the drill (npm install --save fs)
const fs = require('fs');

// Include controllers
fs.readdirSync("controllers").forEach((file) => {
  if (file.substr(-3) == ".js") {
    const route = require("./controllers/"+file);
    route.controller(router); // In the book: route.controller(app)
    // But it is weird as router is then used to do the same things for get '/' just after
  }
})
```

We're supposed to have everything good to try and make a request (using a tool or by going to our AddMovie page). However, you will have an issue if you try because express need a module to extract the body of the request to expose it to req.body. 

We will add a little line in `server.js` just after the creation of the express instance:

```javascript
app.use(express.json()); 

// bodyParser has been incorporated into express in release 4.16.0
// Before : app.use(bodyParser.json())
```

Everything should work now!
You can now try making a request and if everything work, an Object containing the movie you added should display in your terminal where you launched `server.js`.
Remember that you need at this time to launch 3 terminals:
* `npm run serve` that launches the front-end with Vue
* `sudo mongod` that launches the mongo server
* `npm start` that launches the backend server with Express

![POST new movie screenshot](./postREST.png "POST New Movie with REST Client")


You can also check in the mongo CLI that the movie has been correctly added


```shell
$ mongo
$ >use movie_app
switched to db movie_app
$ >db.movies.find()
{ "_id" : ObjectId("5eac4e6b9425873e84cbcfb2"), "name" : "hello", "__v" : 0 }
```

* _id is the value that makes this document unique. So even if we add another document with name: "hello", the id will be different.
* [What is __v ?](https://mongoosejs.com/docs/guide.html#versionKey)


__If that does not work, check in Issues : Axios__



## Home / Movie list

- Write Movie.vue (v-card)
	With mounted() -> this.fetchMovie GET (/movies/movie_id)
- Write Home.vue (grid with v-row)
	With mounted() -> this.fetchMovies GET (/movies)
- In the controller movie.js add the two new API specifications
	find({}, 'name ... genre') -> SELECT name .. genre from *
	findById({req.params.id}, 'name .. genre') -> SELECT name .. genre WHERE id=id


## EditMovie

- Add a button in Movie.vue that routes to EditMovie
- Create component EditMovievue with similar form to AddMovie
- Add a function initForm() that will call an axios GET method to movies/:id,
then take the response (which is a movie) and use the information to initialize data. This function will be called in mounted().
- Your submit function must call a PUT method (modify) and not a POST method (add new).
- Create corresponding API endpoint.
- Write endpoint in routes/index.js

# Debugging

In controllers / server.js
=> Make request with curl or Postman/RESTClient or similar application
=> Check if you need to have 'Content-Type':'application/json' as header
=> You can check then the response for status and/or error message.
=> Error messages will be displayed in your terminal where the server has been launched too
=> console.log() in these files will be display on that terminal.


# Accessibility notes

Is v-select adapted for an accessible system ?
It is not obvious but you can still write an answer and use up/down key to reach your value.
An alternative choice is v-combobox.

# Issues

## RunTime Compiler

Error: 
[Vue warn]: You are using the runtime-only build of Vue where the template compiler is not available. Either pre-compile the templates into render functions, or use the compiler-included build.
https://cli.vuejs.org/config/#runtimecompiler

>Adding `runtimeCompiler: true` fixes my error but I don't really understand why I need that as I do not pass a template string.


## Axios

Error: "Request failed with status code 404"

>First try using a REST Client to make the request manually. If it does work, then it is axios fault. Be sure to have the same url in axios and in the router.

I need to put the full url in axios:
url: 'http://localhost:3000/api/movies' to have it recognized by the router as '/api/movies'
The book uses serve static

```javascript
const serveStatic = require('serve-static');

app.use(serveStatic(__dirname + "/dist"));
```

Then you don't need to use npm run serve / npm run dev anymore. But you have to build each type you modify a vue file.
It says you don't need the /api prefix anymore since we only have one port (the one of the backend). However, it works even if you don't have /api prefix as long as axios request url and the router api is identic ofc.
I don't really understand what the use of /api prefix is, and why it is useful to do serve static.
!!! Need to try and figure out what is the use and serve static and why does I need the /api prefix!!!

Once I have done that, I stumbled into a new issue. the submit button send an OPTION request.
After research, I need cors which is used in the book. It says it manages cross-origin requests between frontend and backend.
[Why is my browser sending OPTION instead of POST](https://dev.to/p0oker/why-is-my-browser-sending-an-options-http-request-instead-of-post-5621)
!!! Need to figure out what the issue is and what is CORS !!!

>Install cors and add it to express
```javascript
// In server.js
const cors = require('cors');

app.use(cors());
```

## Vue: Avoid using non primitive value as key

https://stackoverflow.com/questions/53420082/vue-warn-avoid-using-non-primitive-value-as-key-use-string-number-value-inst

> v-for="movie in movies"
  :key="movie"

->

> v-for="(movie, idx) in movies"
  :key="idx"
